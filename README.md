# Clock Timer

Some code written for fun

## Install requeriments
```
$ python -m venv venv
$ . venv/bin/activate
$ pip install -r requeriments.txt
```

## Run the program
```

$ python main.py \
    --rounds=3 \
    --ejercicios=9 \
    --tiempo=45 \
    --descanso=20 \
    --pausa=60

```

### Params

- **rounds**: number of rounds
- **ejercicios**: number of exercices
- **tiempo**: number of seconds to exercice
- **descanso**: number of seconds before new exercice
- **pausa**: number of seconds before new round
