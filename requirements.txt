click==7.1.1
entrypoints==0.3
flake8==3.7.9
mccabe==0.6.1
pycodestyle==2.5.0
pydub==0.23.1
pyfiglet==0.8.post1
pyflakes==2.1.1
