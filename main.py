#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  main.py
#
#  Copyright 2020 Alexis Sostersich <alexis.sostersich@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import click
import os
import pyfiglet
import time

from pydub import AudioSegment
from pydub.playback import play


def get_time():
    return time.strftime('')


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def echo(texto, cache=[], clear=False):
    return pyfiglet.figlet_format(texto)


def sleep_timer(seconds, lineas):
    for t in list(range(seconds+1))[::-1]:
        lineas.append(echo(f"    {t}"))
        cls()
        [click.echo(l) for l in lineas]
        time.sleep(1)
        del lineas[-1]


sonidos = {
    'nuevo_ejercicio': 'sound/nuevo_ejercicio.mp3',
    'descanso': 'sound/descanso.mp3',
    'fin_ejercicio': 'sound/fin_ejercicio.mp3',
}


@click.command()
@click.option("--rounds", default=3, help="Cantidad de rounds")
@click.option("--ejercicios", default=10, help="Cantidad de ejercicios")
@click.option("--tiempo", default=30, help="Tiempo por ejercicio")
@click.option("--descanso", default=0, help="Descanso entre ejercicios")
@click.option("--pausa", default=60, help="Pausa/Descanso entre rounds")
def timer(rounds, ejercicios, tiempo, descanso, pausa):
    """Simple program that greets NAME for a total of COUNT times."""
    for circuito in range(1, rounds+1):
        lineas = []
        lineas.append('*' * 60)
        lineas.append(echo(f"Ciurcuito {circuito} / {rounds}"))
        lineas.append('*' * 60)
        for e in range(1, ejercicios+1):
            play(AudioSegment.from_mp3(sonidos['nuevo_ejercicio']))
            lineas.append(echo(f"Ejercicio {e} / {ejercicios}"))
            sleep_timer(tiempo, lineas)
            del lineas[-1]
            play(AudioSegment.from_mp3(sonidos['fin_ejercicio']))
            if descanso > 0 and e < ejercicios:
                play(AudioSegment.from_mp3(sonidos['descanso']))
                lineas.append(echo(f"Descanso {descanso}''"))
                sleep_timer(descanso, lineas)
                del lineas[-1]
        if pausa > 0 and circuito < rounds:
            play(AudioSegment.from_mp3(sonidos['descanso']))
            lineas.append(echo(f"Descanso {pausa}''"))
            sleep_timer(pausa, lineas)
            del lineas[-1]


if __name__ == '__main__':
    timer()
